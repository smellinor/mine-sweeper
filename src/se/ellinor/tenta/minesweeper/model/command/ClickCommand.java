package se.ellinor.tenta.minesweeper.model.command;

import se.ellinor.tenta.minesweeper.model.board.GameBoard;
import se.ellinor.tenta.minesweeper.model.component.MineSweeperComponent;
import se.ellinor.tenta.minesweeper.model.component.factory.MineSweeperComponentFactoryType;

public class ClickCommand implements Command {

	private GameBoard b;
	private int x;
	private int y;
	private MineSweeperComponentFactoryType fac;

	public ClickCommand(GameBoard b, int x, int y, MineSweeperComponentFactoryType fac) {
		this.b = b;
		this.x = x;
		this.y = y;
		this.fac = fac;
	}

	@Override
	public void execute() {
		this.b.click(this.x, this.y);
	}

	@Override
	public void undo() {
		MineSweeperComponent c = this.fac.getBlock(this.x, this.y);
		c.wrap(b.get(this.x, this.y));
		this.b.add(this.x, this.y, c);	
	}

}
