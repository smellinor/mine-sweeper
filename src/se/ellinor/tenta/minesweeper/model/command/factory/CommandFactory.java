package se.ellinor.tenta.minesweeper.model.command.factory;

import se.ellinor.tenta.minesweeper.model.board.GameBoard;
import se.ellinor.tenta.minesweeper.model.command.Command;

public interface CommandFactory {
	
	public Command getClickCommand(GameBoard b, int x, int y);
	public Command getPrintCommand(GameBoard b);

}
