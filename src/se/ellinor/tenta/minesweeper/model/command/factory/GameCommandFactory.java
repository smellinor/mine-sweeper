package se.ellinor.tenta.minesweeper.model.command.factory;

import se.ellinor.tenta.minesweeper.model.board.GameBoard;
import se.ellinor.tenta.minesweeper.model.command.ClickCommand;
import se.ellinor.tenta.minesweeper.model.command.Command;
import se.ellinor.tenta.minesweeper.model.command.PrintCommand;
import se.ellinor.tenta.minesweeper.model.component.factory.MineSweeperComponentFactory;

public class GameCommandFactory implements CommandFactory {

	@Override
	public Command getClickCommand(GameBoard b, int x, int y) {
		return new ClickCommand(b, x, y, new MineSweeperComponentFactory());
	}

	@Override
	public Command getPrintCommand(GameBoard b) {
		return new PrintCommand(b);
	}

}
