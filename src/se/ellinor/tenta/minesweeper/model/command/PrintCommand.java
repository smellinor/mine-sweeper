package se.ellinor.tenta.minesweeper.model.command;

import se.ellinor.tenta.minesweeper.model.board.GameBoard;

public class PrintCommand implements Command {
	
	private GameBoard b;

	public PrintCommand(GameBoard b) {
		this.b = b;
	}

	@Override
	public void execute() {
		System.out.println(b);
	}

	@Override
	public void undo() {

	}

}
