package se.ellinor.tenta.minesweeper.model.command;

public interface Command {
	
	public void execute();
	public void undo();

}
