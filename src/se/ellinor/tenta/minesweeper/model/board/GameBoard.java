package se.ellinor.tenta.minesweeper.model.board;

import se.ellinor.tenta.minesweeper.model.component.MineSweeperComponent;

public interface GameBoard {
	
	public void add(int x, int y, MineSweeperComponent c);
	public MineSweeperComponent get(int x, int y);
	public void click(int x, int y);
	public boolean lost();

}
