package se.ellinor.tenta.minesweeper.model.board.factory;

import se.ellinor.tenta.minesweeper.model.board.GameBoard;

public interface MineSweeperFactoryType {
	
	public GameBoard getBoard();

}
