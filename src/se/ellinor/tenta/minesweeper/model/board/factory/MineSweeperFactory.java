package se.ellinor.tenta.minesweeper.model.board.factory;

import se.ellinor.tenta.minesweeper.model.board.Board;
import se.ellinor.tenta.minesweeper.model.board.GameBoard;
import se.ellinor.tenta.minesweeper.model.component.factory.MineSweeperComponentFactory;

public class MineSweeperFactory implements MineSweeperFactoryType {

	@Override
	public GameBoard getBoard() {
		return new Board(new MineSweeperComponentFactory());
	}

}
