package se.ellinor.tenta.minesweeper.model.board;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.ellinor.tenta.minesweeper.model.component.Mine;
import se.ellinor.tenta.minesweeper.model.component.MineSweeperComponent;
import se.ellinor.tenta.minesweeper.model.component.factory.MineSweeperComponentFactoryType;

public class Board implements GameBoard {

	private int size;
	private Map<Integer, List<MineSweeperComponent>> board;

	public Board(MineSweeperComponentFactoryType fac) {
		this(7, fac);
	}
	
	public Board(int s, MineSweeperComponentFactoryType fac) {
		this.size = s;
		this.board = new HashMap<>();
		for(int i = 0; i < size ; i++) {
			List<MineSweeperComponent> col = new ArrayList<>();
			for(int j = 0 ; j < size ; j++) {
				MineSweeperComponent block = fac.getBlock(i, j);
				col.add(j, block);
				if(mine())
					block.wrap(fac.getEmptyBlock(i, j));
				else
					block.wrap(fac.getMine(i, j));
			}
			this.board.put(i, col);
		}
	}
	
	@Override
	public void click(int x, int y) {
		MineSweeperComponent c = this.board.get(x).get(y);
		this.board.get(x).remove(c);
		this.board.get(x).add(y, c.peel());
	}

	private boolean mine() {
		return Math.random()*100. < 25.;
	}
	
	@Override
	public void add(int x, int y, MineSweeperComponent c) {
		this.board.get(x).remove(y);
		this.board.get(x).add(y, c);
	}

	@Override
	public MineSweeperComponent get(int x, int y) {
		return this.board.get(x).get(y);
	}

	@Override
	public String toString() {
		String string = "";
		for(int i = 0 ; i < size; i++) {
			string += "|";
			for(int j = 0 ; j < size ; j++) {
				string += this.board.get(i).get(j).toString() + "|";
			}
			string += "\n";
		}
		return string;
	}
	
	@Override
	public boolean lost() {
		for(List<MineSweeperComponent> list : this.board.values())
			for(MineSweeperComponent c : list)
				if(c instanceof Mine)
					return true;
		return false;
	}

}
