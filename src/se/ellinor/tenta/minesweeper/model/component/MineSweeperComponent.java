package se.ellinor.tenta.minesweeper.model.component;

public interface MineSweeperComponent {
	
	public MineSweeperComponent copy();
	public int getX();
	public int getY();
	public void setX(int x);
	public void setY(int y);
	public MineSweeperComponent peel();
	public void wrap(MineSweeperComponent c);

}
