package se.ellinor.tenta.minesweeper.model.component.factory;

import se.ellinor.tenta.minesweeper.model.component.MineSweeperComponent;

public interface MineSweeperComponentFactoryType {

	public MineSweeperComponent getEmptyBlock(int x, int y);
	public MineSweeperComponent getMine(int x, int y);
	public MineSweeperComponent getBlock(int x, int y);
	
}
