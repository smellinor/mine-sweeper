package se.ellinor.tenta.minesweeper.model.component.factory;

import se.ellinor.tenta.minesweeper.model.component.Block;
import se.ellinor.tenta.minesweeper.model.component.EmptyBlock;
import se.ellinor.tenta.minesweeper.model.component.Mine;
import se.ellinor.tenta.minesweeper.model.component.MineSweeperComponent;

public class MineSweeperComponentFactory implements MineSweeperComponentFactoryType {

	@Override
	public MineSweeperComponent getEmptyBlock(int x, int y) {
		return new EmptyBlock(x, y);
	}

	@Override
	public MineSweeperComponent getMine(int x, int y) {
		return new Mine(x, y);
	}

	@Override
	public MineSweeperComponent getBlock(int x, int y) {
		return new Block(x, y);
	}

}
