package se.ellinor.tenta.minesweeper.model.component;

public class EmptyBlock implements MineSweeperComponent {

	private int y;
	private int x;
	
	public EmptyBlock(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public MineSweeperComponent copy() {
		return new EmptyBlock(this.x, this.y);
	}

	@Override
	public int getX() {
		return this.x;
	}

	@Override
	public int getY() {
		return this.y;
	}

	@Override
	public void setX(int x) {
		this.x = x;
	}

	@Override
	public void setY(int y) {
		this.y = y;
	}

	@Override
	public MineSweeperComponent peel() {
		return (MineSweeperComponent) this;
	}

	@Override
	public void wrap(MineSweeperComponent c) {
		
	}
	
	@Override
	public String toString() {
		return " ";
	}

}
