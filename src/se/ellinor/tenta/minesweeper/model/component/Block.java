package se.ellinor.tenta.minesweeper.model.component;

public class Block implements MineSweeperComponent {

	private MineSweeperComponent wrapee;
	private int x;
	private int y;

	public Block(MineSweeperComponent w) {
		this.x  = w.getX();
		this.y = w.getY();
		this.wrapee = w.copy();
	}

	public Block(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public MineSweeperComponent peel() {
		return (MineSweeperComponent) this.wrapee;
	}

	@Override
	public MineSweeperComponent copy() {
		return new Block(this.wrapee);
	}

	@Override
	public int getX() {
		return this.x;
	}

	@Override
	public int getY() {
		return this.y;
	}

	@Override
	public void setX(int x) {
		this.x = x;
	}

	@Override
	public void setY(int y) {
		this.y = y;
	}

	@Override
	public void wrap(MineSweeperComponent c) {
		if(c.getX() == this.x && c.getY() == this.y)
			this.wrapee = c;
		else
			throw new IndexOutOfBoundsException();
	}
	
	@Override
	public String toString() {
		return "+";
	}

}
