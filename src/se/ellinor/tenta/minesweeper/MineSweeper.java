package se.ellinor.tenta.minesweeper;

import java.util.Scanner;
import java.util.Stack;

import se.ellinor.tenta.minesweeper.model.board.GameBoard;
import se.ellinor.tenta.minesweeper.model.board.factory.MineSweeperFactory;
import se.ellinor.tenta.minesweeper.model.command.Command;
import se.ellinor.tenta.minesweeper.model.command.factory.CommandFactory;
import se.ellinor.tenta.minesweeper.model.command.factory.GameCommandFactory;

public class MineSweeper implements Game {

	private GameBoard board;
	private Stack<Command> done;
	private Stack<Command> undone;
	private CommandFactory comm;

	public MineSweeper() {
		this.comm = new GameCommandFactory();
		this.board = new MineSweeperFactory().getBoard();
		this.done = new Stack<>();
		this.undone = new Stack<>();
	}

	@Override
	public void click(int x, int y) {
		Command c = this.comm.getClickCommand(this.board, x, y);
		c.execute();
		this.done.add(c);
		this.undone.clear();
	}

	@Override
	public void print() {
		Command c = this.comm.getPrintCommand(this.board);
		c.execute();
		this.done.add(c);
		this.undone.clear();
	}

	@Override
	public void undo() {
		if(!this.done.isEmpty()) {
			Command c = this.done.pop();
			c.undo();
			this.undone.push(c);
		}
	}

	@Override
	public void reset() {
		this.done.clear();
		this.undone.clear();
		this.board = new MineSweeperFactory().getBoard();
	}

	@Override
	public boolean lost() {
		return this.board.lost();
	}

	public static void main(String[]args) throws RuntimeException {
		Scanner in = new Scanner(System.in);
		Game m = new MineSweeper();
		int a  = -1;
		int b  = -1;
		boolean play = true;
		m.print();
		while(play) {
			if(a == -1) {
				a = in.nextInt();
				if(a < 0 || a > 7)
					a = -1;
			} else if(b == -1) {
				b = in.nextInt();
				if(b < 0 || b > 7)
					b = -1;
			} else {
				m.click(a, b);
				if(!m.lost()) {
					m.print();
				} else {
					System.out.println("you hit a mine!!");
					System.out.println();
					System.out.println("new game:");
					m = new MineSweeper();
					m.print();
				}
				a = -1;
				b = -1;
			}
		}
		in.close();
	}

}
