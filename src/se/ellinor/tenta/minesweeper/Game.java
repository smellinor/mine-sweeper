package se.ellinor.tenta.minesweeper;

public interface Game {
	
	public void click(int x, int y);
	public void print();
	public void undo();
	public void reset();
	public boolean lost();

}
